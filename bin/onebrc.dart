import 'dart:collection';
import 'dart:convert';
import 'dart:io';

class Stat {
  double minimum = double.infinity;
  double maximum = double.negativeInfinity;
  double sum = 0.0;
  int count = 0;
}

class LineData {
  final int semicolonIndex;
  final int size;
  final List<int> buffer;

  LineData({
    required this.semicolonIndex,
    required this.size,
    required this.buffer,
  });
}

const dash = 45;
const period = 46;
const zero = 48;
const semicolon = 59;
const newline = 10;

final stats = SplayTreeMap<String, Stat>((a, b) => a.compareTo(b));
final decoder = utf8.decoder;

void main(List<String> arguments) async {
  final buffer = List.filled(128, 0);
  int bufferSize = 0;
  int semicolonIndex = 0;

  final file = File(arguments.isNotEmpty ? arguments[0] : 'measurements.txt');
  await for (final b in file.openRead()) {
    for (int i = 0; i < b.length; i++) {
      final c = b[i];
      if (c != newline) {
        if (c == semicolon) {
          semicolonIndex = bufferSize;
        }
        buffer[bufferSize] = c;
        bufferSize++;
        continue;
      }

      final ld = LineData(
          semicolonIndex: semicolonIndex,
          size: bufferSize,
          buffer: buffer.sublist(0, bufferSize));
      updateStats(ld);
      bufferSize = 0;
    }
  }

  final names = stats.keys.toList();
  stdout.write('{');
  stdout.write(names.map((n) {
    final stat = stats[n]!;
    final mean = stat.sum / stat.count.toDouble();
    return '$n=${stat.minimum}/${mean.toStringAsFixed(1)}/${stat.maximum}';
  }).join(', '));
  stdout.write('}');
}

double byteArrayToValue(List<int> codeUnits) {
  double sign = 1.0;
  int intValue = 0;
  int decimalValue = 0;
  int decimalCount = 0;
  bool intMode = true;
  for (final c in codeUnits) {
    if (c == dash) {
      sign = -1.0;
      continue;
    }

    if (c == period) {
      intMode = false;
      continue;
    }

    final v = c - zero;
    if (intMode) {
      intValue = intValue * 10 + v;
    } else {
      decimalValue = decimalValue * 10 + v;
      decimalCount++;
    }
  }
  return sign * (intValue + decimalValue / (10.0 * decimalCount));
}

void updateStats(LineData data) {
  final name = decoder.convert(data.buffer, 0, data.semicolonIndex);
  final value =
      byteArrayToValue(data.buffer.sublist(data.semicolonIndex + 1, data.size));

  final stat = stats.putIfAbsent(name, () => Stat());
  if (value < stat.minimum) {
    stat.minimum = value;
  }
  if (value > stat.maximum) {
    stat.maximum = value;
  }
  stat.sum = stat.sum + value;
  stat.count = stat.count + 1;
}
